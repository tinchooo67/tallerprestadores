package com.project.osde.prestadores;

import java.util.concurrent.TimeUnit; 

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BasePrueba {
		
		public WebDriver driver;
		public WebDriverWait wait;

		@BeforeTest
		public void setUp() throws InterruptedException {
			//seteamos las propiedades del chromedriver
			System.setProperty("webdriver.chrome.driver","./src/test/resources/chromedriver/chromedriver.exe");
			
			// Creamos una nueva instancia del chromedriver
			driver = new ChromeDriver();

			//Se configura el wait implicito en 5 segundos
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			//Accedemos a la pagina fr prestadores
			driver.get("https://tsegundaopinion.osde.com.ar/prestadores/signin");
			
			// Mostramos en consola un mensaje
			System.out.println("Se accedio a la pagina web");

			//Se configura el wait explicito en 10 segundos
			wait = new WebDriverWait(driver, 10);

		}
		
		@AfterTest
		public void tearDown(){
			// Cerramos el driver y el browser
			driver.close();
			driver.quit();
		}

	}


