package com.project.osde.prestadores;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginPage {
	
	public WebDriver driver;
	private WebDriverWait wait;
	
	@FindBy(name="user")
	WebElement usr;
	
	@FindBy(name="password")
	WebElement pass;
	
	@FindBy(xpath="//*[@id=\"prestadores-signin-btn-login\"]")
	WebElement btnIngresar;
	
	@FindBy(className = "toast-message")
	private WebElement alertaMensajeLogin;
	
	public LoginPage(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);	
	}
	
	public void IngresarUsuario(String usuario) {
		wait.until(ExpectedConditions.visibilityOf(usr));
		Assert.assertTrue(usr.isDisplayed());
		usr.sendKeys(usuario);
	}
	
	public void IngresarPass(String password){
		Assert.assertTrue(pass.isDisplayed());
    	pass.sendKeys(password);
    	
    }

    public void clickLogin(){
    	Assert.assertTrue(btnIngresar.isDisplayed());
    	btnIngresar.click();
    }
   
	
    public void login(String usuario, String password) {
    	IngresarUsuario(usuario);
    	IngresarPass(password);
    	clickLogin();
    	clicMensajeAlertaLogin("Bienvenido!");
    }

	public void clicMensajeAlertaLogin(String msj) {
		wait.until(ExpectedConditions.textToBePresentInElement(alertaMensajeLogin, msj));
		Assert.assertTrue(alertaMensajeLogin.isDisplayed());

		
	}

}
