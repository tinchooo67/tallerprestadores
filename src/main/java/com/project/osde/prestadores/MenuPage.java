package com.project.osde.prestadores;

import static org.testng.Assert.assertTrue; 

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class MenuPage {
	public WebDriver driver;
	private WebDriverWait wait;

	@FindBy (xpath ="//*[@id=\"main\"]/div/div/div[2]/div/div/div[1]/div/div/div/button")
	WebElement Btninterconsulta;

	@FindBy (xpath = "//*[@id=\"navbar\"]/ul/li[1]/a")
	WebElement bienvenida;

	@FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
	WebElement cambiopass;

	@FindBy(id = "btSalir")
	WebElement salir;
	
	
	public MenuPage(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
		PageFactory.initElements(driver, this);
	}

	public void Validaciones() {
		Assert.assertTrue(Btninterconsulta.isDisplayed());
		Assert.assertEquals(bienvenida.getText(),"Bienvenido, Usuario Test03");
		Assert.assertTrue(cambiopass.isDisplayed());
		Assert.assertTrue(salir.isDisplayed());
		
	}

}


