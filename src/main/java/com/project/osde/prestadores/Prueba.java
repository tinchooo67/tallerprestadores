package com.project.osde.prestadores;


import org.testng.Assert; 
import org.testng.annotations.Test;


public class Prueba extends BasePrueba {
	
	
	@Test
	public void login() throws InterruptedException {
		//Se verifica que la url sea https://tsegundaopinion.osde.com.ar/prestadores/signin
		Assert.assertEquals(driver.getCurrentUrl(), "https://tsegundaopinion.osde.com.ar/prestadores/signin");
		LoginPage login = new LoginPage(driver, wait);
		login.login("jmortiz@sms-latam.com", "Smotest1!");
	}

	@Test (dependsOnMethods = {"login"})
	public void home() throws InterruptedException {
		MenuPage menu = new MenuPage(driver, wait);
		menu.Validaciones();
	
	}

	@Test (dependsOnMethods = {"home"})
	public void invitacion() throws InterruptedException {
		InterconsultaPage admin = new InterconsultaPage(driver, wait);
		admin.Cargadedatos();
		
	}
	
}

	

