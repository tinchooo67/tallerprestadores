package com.project.osde.prestadores;

import java.util.List;  

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class InterconsultaPage {
	
	public WebDriver driver;
	private WebDriverWait wait;
	
	@FindBy (xpath ="//*[@id=\"main\"]/div/div/div[2]/div/div/div[1]/div/div/div/button")
	WebElement Btninterconsulta;
	
	@FindBy (xpath="//*[@id=\"main\"]/div/div/div/div/div[1]/button")
    WebElement Btninvitacion;
	
	@FindBy (xpath="//*[@id=\"main\"]/div/div/div/div/div[2]/div/div[1]")
    WebElement planilla;
	
	@FindBy (name="valorIdentificacion")
	WebElement socio;
	
	@FindBy (id="apellido")
	WebElement apellido;
	
	@FindBy (id="nombre")
	WebElement nombre;
	
	@FindBy (id="fechaNacimiento")
	WebElement fechanac;
	
	@FindBy (id="apellidoSolicitante")
	WebElement apellidosol;
	
	@FindBy (id="nombreSolicitante")
	WebElement nombresol;
	
	@FindBy (id="tipoMatricula")
	WebElement tipomatricula;
	
	@FindBy (id="nroMatricula")
	WebElement nromatricula;
	
	@FindBy (id="email")
	WebElement mail;
	
	@FindBy(id= "phone")
	WebElement phone;
	
	@FindBy(name="detalle")
	WebElement campomotivo;
	
	@FindBy(id="especialidad")
	WebElement tipoespecialidad;
	
	@FindBy(id="profesional")
	WebElement profesional;
	
	@FindBy(xpath="//*[@id=\"enviarInvitacionForm\"]/div/footer/div/div/div/div[2]/button")
	WebElement Btnenviar;
	
	
	
	public InterconsultaPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }
	
	public void Cargadedatos() {
		Btninterconsulta.click();
		Assert.assertTrue(Btninvitacion.isDisplayed());
		Btninvitacion.click();
		Assert.assertTrue(planilla.isDisplayed());
		socio.sendKeys("55555555555");
		apellido.sendKeys("Ortiz");
		nombre.sendKeys("Juancho");
		fechanac.click();
		fechanac.sendKeys("06071990");
		apellidosol.sendKeys("Tedesco");
		nombresol.sendKeys("Noelia");
		Select seltipomatricula = new Select(tipomatricula);
		seltipomatricula.selectByValue("2");
		nromatricula.sendKeys("1111111111");
		mail.sendKeys("sarasa@hotmail.com");
		phone.sendKeys("1137627866");
		Select seltipoespecialidad = new Select(tipoespecialidad);
		seltipoespecialidad.selectByValue("4");
		Select selprofesional = new Select(profesional);
		selprofesional.selectByValue("329");
		campomotivo.sendKeys("Probando automatizar");
		Btnenviar.click();
		
		
	}




}
